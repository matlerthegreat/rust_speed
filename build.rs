fn main() {
    let dst = cmake::build("native");
    // Tell cargo to look for shared libraries in the specified directory
    println!("cargo:rustc-link-search={}", dst.display());

    println!("cargo:rustc-link-lib=cpp");
    //println!("cargo:rustc-link-lib=stdc++");

    // Tell cargo to invalidate the built crate whenever the wrapper changes
    println!("cargo:rerun-if-changed=native");
}