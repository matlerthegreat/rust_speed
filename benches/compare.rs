
use criterion::{criterion_group, criterion_main, Criterion, BenchmarkId, black_box};
use rust_speed::{rust_simple, gen_input, cpp_simple, cpp_chunked};

const SIZES: [usize; 4] = [100, 500, 1000, 5000];

pub fn compare(c: &mut Criterion) {
    let mut group = c.benchmark_group("compare");

    for size in SIZES.iter() {
        group.bench_with_input(BenchmarkId::new("rust_simple", size), size, 
            |b, size| {
                let mut rng = rand::thread_rng();

                let xs = gen_input(&mut rng, *size);
               
                b.iter(|| rust_simple(black_box(&xs)))
            });

        group.bench_with_input(BenchmarkId::new("cpp_simple", size), size, 
            |b, size| {
                let mut rng = rand::thread_rng();

                let xs = gen_input(&mut rng, *size);
                
                b.iter(|| cpp_simple(black_box(&xs)))
            });

        group.bench_with_input(BenchmarkId::new("cpp_chunked", size), size, 
            |b, size| {
                let mut rng = rand::thread_rng();

                let xs = gen_input(&mut rng, *size);
                
                b.iter(|| cpp_chunked(black_box(&xs)))
            });
    }
    group.finish();
}

criterion_group!(benches, compare);
criterion_main!(benches);