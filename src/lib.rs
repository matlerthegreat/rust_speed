extern "C" {
    pub fn simple(xs: *const i32, len: usize) -> usize;
    pub fn chunked(xs: *const i32, len: usize) -> usize;
}

pub fn cpp_simple(xs: &[i32]) -> usize {
    let len = xs.len();
    unsafe {
        simple(xs.as_ptr(), len)
    }
}

pub fn cpp_chunked(xs: &[i32]) -> usize {
    let len = xs.len();
    unsafe {
        chunked(xs.as_ptr(), len)
    }
}


// Count how many collisions there are
pub fn rust_simple(xs: &[i32]) -> usize {
    let pairs = xs.iter().enumerate().flat_map(move |(n, x)| xs.iter().skip(n+1).map(move |y| (x, y)));
    pairs.filter(|(x, y)| x == y).count()
}

pub fn gen_input(rng: &mut impl rand::Rng, n: usize) -> Vec<i32> {
    (0..n).map(|_| rng.gen_range(-10..10)).collect()
}
    

#[cfg(test)]
mod tests {
    use rand::{rngs::SmallRng, SeedableRng};

    use super::*;

    #[test]
    fn test_rust_simple() {
        let xs = [1, 2, 3, 4, 5, 1, 6, 7, 8, 9, 3, 1];
        
        // Count how many collisions there are
        // In this example there are (1, 1), (1, 1), (3, 3), (1, 1) so 4 collisions
        let count = rust_simple(&xs);

        assert_eq!(count, 4);
    }

    #[test]
    fn test_cpp_simple() {
        let xs = [1, 2, 3, 4, 5, 1, 6, 7, 8, 9, 3, 1];
        
        // Count how many collisions there are
        // In this example there are (1, 1), (1, 1), (3, 3), (1, 1) so 4 collisions

        let count = cpp_simple(&xs);

        assert_eq!(count, 4);
    }

    #[test]
    fn test_cpp_chunked() {
        let xs = [1, 2, 3, 4, 5, 1, 6, 7, 8, 9, 3, 1];
        
        // Count how many collisions there are
        // In this example there are (1, 1), (1, 1), (3, 3), (1, 1) so 4 collisions

        let count = cpp_chunked(&xs);

        assert_eq!(count, 4);
    }

    #[test]
    fn compare_random_simple() {
        let mut rng = SmallRng::seed_from_u64(123456789);

        let xs = gen_input(&mut rng, 1000);

        let rust = rust_simple(&xs);
        let cpp = cpp_simple(&xs);

        assert_eq!(rust, cpp);
    }

    #[test]
    fn compare_random_chunked() {
        let mut rng = SmallRng::seed_from_u64(123456789);

        let xs = gen_input(&mut rng, 1000);

        let rust = rust_simple(&xs);
        let cpp = cpp_chunked(&xs);

        assert_eq!(rust, cpp);
    }
}