

#ifdef __cplusplus
extern "C" {
#endif
 
size_t simple(int* xs, size_t len);
size_t chunked(int* xs, size_t len);

#ifdef __cplusplus
}
#endif