#include "cpp.hpp"

#include <iostream>
#include <vector>

// Count how many collisions there are
size_t simple(int *xs, size_t len)
{
    size_t count = 0;

    for (size_t i = 0; i < len; ++i) {
        for (size_t j = i + 1; j < len; ++j) {
            int first = xs[i];
            int second = xs[j];
            if (first == second) {
                ++count;
            }
        }
    }

    return count;
}

size_t chunked(int *xs, size_t len)
{
    size_t chunk = 16;
    size_t chunks = len / chunk;
    
    size_t count = 0;

    for (size_t c = 0; c <= chunks; ++c) {
        size_t begin = c * chunk;
        size_t end = std::min((c+1)*chunk, len);

        // chunk c vs itself
        for (size_t i = begin; i < end; ++i) {
            for (size_t j = i + 1; j < end; ++j) {
                int first = xs[i];
                int second = xs[j];
                if (first == second) {
                    ++count;
                }
            }
        }

        // chunk c vs rest of chunks
        for (size_t d = c + 1; d <= chunks; ++d) {
            size_t begin_d = d * chunk;
            size_t end_d = std::min((d+1)*chunk, len);
            for (size_t i = begin; i < end; ++i) {
                for (size_t j = begin_d; j < end_d; ++j) {
                    int first = xs[i];
                    int second = xs[j];
                    if (first == second) {
                        ++count;
                    }
                }
            }
        }
    }
    
    return count;
}